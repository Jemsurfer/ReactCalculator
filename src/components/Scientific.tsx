import { evaluate } from "mathjs";
import { BaseSyntheticEvent } from "react";

interface Props {
  calc: string;
  setCalc: (calc: string) => void;
  result: string;
  setResult: (result: string) => void;
}

function Scientific({ calc, setCalc, result, setResult }: Props) {
  const ops = [
    "/",
    "*",
    "+",
    "-",
    "sqrt",
    "^",
    "sin",
    "cos",
    "tan",
    "log",
    "derivative",
  ];

  const updateCalc = (value: string) => {
    if (
      (ops.includes(value) && calc === "") ||
      (ops.includes(value) && ops.includes(calc.slice(-1))) ||
      (calc.includes(".") && value === ".")
    ) {
      return;
    }
    setCalc(calc + value);
    if (!ops.includes(value)) {
      setResult(evaluate(calc + value).toString());
    }
  };

  const createDigits = () => {
    let digits = [];
    for (let i = 1; i < 10; i++) {
      digits.push(
        <button onClick={() => updateCalc(i.toString())} key={i}>
          {i}
        </button>
      );
    }
    return digits;
  };

  const calculate = () => {
    try {
      setCalc(evaluate(calc).toString());
    } catch (err) {
      console.error(err);
    }
  };

  const deleteLast = () => {
    if (calc === "") {
      return;
    }
    const value = calc.slice(0, -1);
    setCalc(value);
  };

  const clearAll = () => {
    if (calc === "") {
      return;
    }
    setCalc("");
    setResult("");
  };

  const handleKeyboard = (e: BaseSyntheticEvent) => {
    let val: string = e.target.value;
    setCalc(val);
    try {
      return evaluate(val).toString();
    } catch (err) {
      return err;
    }
  };
  return (
    <div className="App">
      <div className="calculator">
        <div className="display">
          <input
            value={calc || ""}
            onChange={handleKeyboard}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                calculate();
              }
            }}
          />
          <br />
          {result ? <span>({result})</span> : ""}
        </div>
        <div className="digits">
          {createDigits()}
          <button onClick={() => setCalc(calc + 0)}>0</button>
          <button onClick={() => setCalc(calc + ".")}>.</button>
          <button onClick={calculate}>=</button>
        </div>
        <div className="sci operators">
          <button onClick={() => updateCalc("/")}>÷</button>
          <button onClick={() => updateCalc("*")}>×</button>
          <button onClick={() => updateCalc("+")}>+</button>
          <button onClick={() => updateCalc("-")}>-</button>
          <button onClick={deleteLast}>DEL</button>
          <button onClick={clearAll}>AC</button>
          <button onClick={() => updateCalc("sqrt(")}>√</button>
          <button onClick={() => updateCalc("(")}>(</button>
          <button onClick={() => updateCalc(")")}>)</button>
          <button onClick={() => updateCalc("sin(")}>sin</button>
          <button onClick={() => updateCalc("cos(")}> cos</button>
          <button onClick={() => updateCalc("tan(")}> tan</button>
          <button onClick={() => updateCalc("^")}>x^y</button>
          <button onClick={() => updateCalc("log(")}>ln</button>
          <button onClick={() => updateCalc("deg")}>°</button>
          <button onClick={() => updateCalc("pi")}>π</button>
          <button onClick={() => updateCalc("e")}>e</button>
          <button onClick={() => updateCalc("i")}>i</button>
          <button onClick={() => updateCalc("x")}>x</button>
          <button onClick={() => updateCalc("derivative(")}>d/dx</button>
        </div>
      </div>
    </div>
  );
}

export default Scientific;
