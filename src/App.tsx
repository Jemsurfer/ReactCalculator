import Basic from "./components/Basic.tsx";
import Scientific from "./components/Scientific.tsx";
import { useState } from "react";

function App() {
  const [calc, setCalc] = useState("");
  const [result, setResult] = useState("");
  const [mode, setMode] = useState("basic");

  switch (mode) {
    case "basic":
      return (
        <>
          <button onClick={() => setMode("scientific")} className="toggle">
            Switch to scientific
          </button>
          <Basic
            calc={calc}
            setCalc={setCalc}
            result={result}
            setResult={setResult}
          />
        </>
      );
    case "scientific":
      return (
        <>
          <button onClick={() => setMode("basic")} className="toggle">
            Switch to basic
          </button>
          <Scientific
            calc={calc}
            setCalc={setCalc}
            result={result}
            setResult={setResult}
          />
        </>
      );
  }
}

export default App;
